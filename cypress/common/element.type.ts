type CyElement = Cypress.Chainable<JQuery<HTMLElement>>;

export default CyElement;