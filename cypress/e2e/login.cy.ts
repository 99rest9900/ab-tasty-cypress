import LoginPage from "../pageObjects/login-page";

const page = new LoginPage();

beforeEach(() => {
  cy.visit(page.url);
})

describe('Login feature', () => {

  it('Successful login with valid email and password @Login_01', () => {
    cy.fixture('common-data').then(data => {
      // fill the form with the valid data and submit
      page.loginInput.type(data.ordinary_creds.login);
      page.passwordInput.type(data.ordinary_creds.password);
      page.signInBtn.click();
      // check that redirected to ab tasty platform 
      cy.url().should('eq', data.platform_url);
    });
  });

  it('Successful login with SSO (redirect to SSO form) @Login_04_1', () => {
    cy.fixture('common-data').then(data => {
      page.loginInput.type(data.sso_creds.login);
      page.ssoSignInBtn.click();
      // check that redirected to organisation login form
      cy.url().should('eq', data.sso_url);
      // here should be steps to fill organisation login form and submit data
      // check that redirected to ab tasty platform 
      cy.url().should('eq', data.platform_url);
    });
  });

  it('Reset password link should lead to reset password form', () => {
    cy.fixture('common-data').then(data => {
      page.resetPasswordLink.click();
      // check that redirected to reset password form
      cy.url().should('eq', data.reset_password_url);
      // check that title is correst
      page.title.should('have.text', 'Reset your password');
    });
  });
});