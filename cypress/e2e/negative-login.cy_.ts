import LoginPage from "../pageObjects/login-page";

const page = new LoginPage();

before(() => {
  cy.visit(page.url);
})

describe('Login feature', () => {

  it('Successful login with valid email and password @Login_01', () => {
    cy.fixture('common-data').then(data => {
      // fill the form with the valid data and submit
      page.loginInput.type(data.ordinary_creds.login);
      page.passwordInput.type(data.ordinary_creds.password);
      page.signInBtn.click();
      // check that redirected to ab tasty platform 
      cy.url().should('eq', data.platform_url);
    });
  });

  it('Successful login with SSO (redirect to SSO form) @Login_04_1', () => {
    cy.fixture('common-data').then(data => {
      page.loginInput.type(data.sso_creds.login);
      page.ssoSignInBtn.click();
      // check that redirected to organisation login form
      cy.url().should('eq', data.sso_url);
      // here should be steps to fill organisation login form and submit data
      // check that redirected to ab tasty platform 
      cy.url().should('eq', data.platform_url);
    });
  });

  it('Successful login with Google account form (not logged in Google) @Login_04_2', () => {
    cy.fixture('common-data').then(data => {
      Cypress.config('baseUrl', )
      page.loginInput.type(data.sso_creds.login);
      page.ssoSignInBtn.click();
      // check that redirected to organisation login form
      cy.url().should('eq', data.sso_url);
      // here should be steps to fill organisation login form and submit data
      // check that redirected to ab tasty platform 
      cy.url().should('eq', data.platform_url);
    });
  });
});