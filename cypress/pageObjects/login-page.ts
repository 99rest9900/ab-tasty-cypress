import CyElement from "../common/element.type";

class LoginPage {
  _url: string;

  constructor() {
    this._url = 'login';
  }

  get url(): string {
    return this._url;
  }

  get title(): CyElement {
    return cy.get('h1');
  }

  get loginInput(): CyElement {
    return cy.get('#email');
  }

  get passwordInput(): CyElement {
    return cy.get('#password');
  }

  get signInBtn(): CyElement {
    return cy.get('[type="submit"]');
  }

  get googleSsoBtn(): CyElement {
    return cy.get('#GOOGLE_SIGN_IN_BUTTON');
  }

  get ssoSignInBtn(): CyElement {
    return cy.get('[type="submit"]').contains('Ok');
  }

  get resetPasswordLink(): CyElement {
    return cy.get('a').contains('Forgot your password?');
  }
}

export default LoginPage;