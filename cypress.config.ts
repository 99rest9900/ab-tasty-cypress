import { defineConfig } from 'cypress'

export default defineConfig({
  e2e: {
    baseUrl: 'https://app2.abtasty.com',
    reporter: 'junit',
    reporterOptions: {
      mochaFile: 'results/index.xml',
      toConsole: true,
    },
  },
})
